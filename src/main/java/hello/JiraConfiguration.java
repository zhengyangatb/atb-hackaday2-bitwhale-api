package hello;

import feign.Response;
import feign.auth.BasicAuthRequestInterceptor;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

@Configuration
public class JiraConfiguration {
    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
        return new BasicAuthRequestInterceptor("cheng.ustc@gmail.com", "Vy8Wi6wk23x1rcVN8ZmGB815");
    }

    @Bean
    public ErrorDecoder errorDecoder() {
        return (methodKey, response) -> new HttpClientErrorException(HttpStatus.valueOf(response.status()));
    }
}
