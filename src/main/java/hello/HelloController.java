package hello;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    final JiraApi jira;
    final PubsubOutboundGateway messagingGateway;
    final ObjectMapper jackson;

    @Autowired
    public HelloController(JiraApi jira, PubsubOutboundGateway messagingGateway, ObjectMapper jackson) {
        this.jira = jira;
        this.messagingGateway = messagingGateway;
        this.jackson = jackson;
    }

//    @GetMapping("/")
//    public String publish(@RequestParam("msg") String message) {
//        messagingGateway.sendToPubsub(message);
//        return "Greetings from Spring Boot!";
//    }

//    @GetMapping("/issue")
//    public String createTicket() {
//        Issue fields = new Issue();
//        fields.setProject(new Identification("10000"));
//        fields.setIssueType(new Identification("10001"));
//        fields.setSummary("From Spring Boot");
//        fields.setDescription("From Sprint Boot");
//        List<String> labels = new ArrayList<>();
//        fields.setLabels(labels);
//        JiraApi.Response response = jira.createTicket(new JiraApi.Request(fields));
//        System.out.println(response);
//        return "success";
//    }
}
