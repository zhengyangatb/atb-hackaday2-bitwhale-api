package hello;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gcp.pubsub.support.BasicAcknowledgeablePubsubMessage;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.MessageHandler;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MessageDistributor {
    final JiraApi jira;

    @Autowired
    public MessageDistributor(JiraApi jira) {
        this.jira = jira;
    }

    @Bean
    @ServiceActivator(inputChannel = "pubsubInputChannel")
    public MessageHandler messageReceiver() {
        return message -> {
            BasicAcknowledgeablePubsubMessage msg = message.getHeaders().get(GcpPubSubHeaders.ORIGINAL_MESSAGE, BasicAcknowledgeablePubsubMessage.class);
            ObjectMapper jackson = new ObjectMapper();
            try {
                jira.createTicket(new JiraApi.Request(
                        jackson.readValue(msg.getPubsubMessage().getData().toStringUtf8(), Map.class)
                ));
                msg.ack();
            } catch (Exception e){
                e.printStackTrace();
            }
        };
    }
}
