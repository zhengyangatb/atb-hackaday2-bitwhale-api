package hello;

import lombok.Data;
import lombok.Value;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

@FeignClient(name = "JIRA", url = "https://bitwhale.atlassian.net/rest/api/2", configuration = JiraConfiguration.class)
public interface JiraApi {
   @PostMapping("/issue")
   Response createTicket(Request fields);

   @Data
   class Request {
      Issue fields;

      public Request(Map<String, String> properties) {
         fields = new Issue();
         fields.setProject(new Identification(properties.remove("projectId")));
         fields.setIssueType(new Identification("10001"));
         fields.setSummary(properties.remove("title"));
         String description = properties.remove("description");
         StringBuilder builder = new StringBuilder();
         for (Map.Entry<String, String> entry : properties.entrySet()) {
            builder.append("- h2.").append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
         }
         builder.append("h3.Detail\n").append(description);
         fields.setDescription(builder.toString());
      }
   }

   @Data
   class Response {
      String id;
      String key;
      String self;
   }
}
