package hello;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Issue {
    private Identification project;

    @JsonProperty("issuetype")
    private Identification issueType;

    private String summary;
    private String description;
    private List<String> labels;
}